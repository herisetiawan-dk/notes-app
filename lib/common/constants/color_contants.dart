import 'package:flutter/material.dart';

class ColorConstants {
  static Color primaryColor = Colors.lightBlue[400];
  static const secondaryColor = Color(0xff484848);
}
