import 'package:flutter/material.dart';

class ColorEntity {
  String name;
  Color value;

  ColorEntity({
    this.name,
    this.value,
  });
}
