import 'package:flutter/material.dart';
import 'package:notes/domain/entities/note_content_entity.dart';
import 'package:notes/domain/entities/note_item_entity.dart';
import 'package:notes/domain/entities/task_entity.dart';

List<NoteItemEntity> dummyNoteItems = [
  NoteItemEntity(
    key: '12345',
    category: 'Private',
    title: 'Ini Judul 1',
    titleColor: Colors.grey,
    bgColor: Colors.red,
    contents: [
      NoteContentPlainEntity(
        key: 'keyencjds',
        textColor: Colors.black,
        textAlign: TextAlign.center,
        fontSize: 15.0,
        text:
            'Lorem ipsum dolor sir amet Lorem ipsum dolor sir amet Lorem ipsum dolor sir amet Lorem ipsum dolor sir amet Lorem ipsum dolor sir amet Lorem ipsum dolor sir amet Lorem ipsum dolor sir amet Lorem ipsum dolor sir amet',
      ),
      NoteContentTaskEntity(
        key: 'dejinusi',
        boxColor: Colors.red,
        textColor: Colors.black,
        taskEntities: [
          TaskEntity(
            checked: true,
            text: 'Task satu beres',
          ),
          TaskEntity(
            checked: false,
            text: 'Task dua belom',
          ),
          TaskEntity(
            checked: true,
            text: 'Task tiga beres',
          ),
        ],
      ),
    ],
  ),
  NoteItemEntity(
    key: '54321',
    category: 'Private',
    title: 'Curhatan Mantan',
    titleColor: Colors.grey,
    bgColor: Colors.green,
    contents: [],
  ),
  NoteItemEntity(
    key: '11111',
    category: 'Work',
    title: 'Next Sprint',
    bgColor: Colors.blue,
    titleColor: Colors.grey,
    contents: [],
  ),
];
