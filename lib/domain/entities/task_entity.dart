class TaskEntity {
  final bool checked;
  final String text;

  TaskEntity({this.checked, this.text});
}
