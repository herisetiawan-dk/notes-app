import 'package:flutter/material.dart';
import 'package:notes/common/constants/layout_contants.dart';

class NoteCardWidget extends StatelessWidget {
  final Widget child;

  const NoteCardWidget({Key key, this.child}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(LayoutConstants.dimen_10),
      child: Container(
        padding: EdgeInsets.all(LayoutConstants.dimen_15),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(LayoutConstants.dimen_10),
        ),
        width: MediaQuery.of(context).size.width * 0.95,
        child: child,
      ),
    );
  }
}
