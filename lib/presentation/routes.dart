import 'package:flutter/material.dart';
import 'package:notes/common/navigation/custom_routes.dart';
import 'package:notes/presentation/journey/notes_detail_screen/note_detail_routes.dart';
import 'package:notes/presentation/journey/notes_screen/notes_routes.dart';

class Routes {
  static Map<String, WidgetBuilder> _routeList = {
    ...NotesRoutes.getAll(),
    ...NoteDetailRoutes.getAll(),
  };

  static Map<String, WidgetBuilder> getAll() => _routeList;

  static Route<dynamic> generateRotes(RouteSettings settings) {
    return CustomRoute.switcher(settings);
  }
}
